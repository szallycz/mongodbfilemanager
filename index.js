const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const database = require('./src/config/configDatabase')
const app = express();
app.use(cors())
//routes
const fileRouter = require('./src/routes/file')
const collectionRouter = require('./src/routes/collections')

app.use(express.json())
app.use('/v1',fileRouter)
app.use('/collections',collectionRouter)
mongoose.connect(database.url)
.then(() => console.log('Connected to MongoDB!'))
.catch(error => console.error('Could not connect to MongoDB... ', error));
mongoose.pluralize(null)
app.listen(database.port, () => console.log("Listening on port " + database.port + "..."));