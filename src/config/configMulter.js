const multer = require("multer");
const config={
    allowed_files:/()/,
    max_upload_multiple:50,
}
const uploadFile = multer({
    fileFilter: (req, file, cb) => {
      // Kontrola typu souboru
      if (config.allowed_files.test(file.mimetype)) {
        // Povolení nahrání souboru
        cb(null, true);
      } else {
        // Nepovolený typ souboru
        cb(
          new Error("Povoleny jsou pouze soubory typu:" + config.allowed_files),
          false
        );
      }
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    },
  });
  
module.exports = {config,uploadFile}