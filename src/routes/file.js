const express = require("express");
const router = express.Router();
const {config,uploadFile} = require("../config/configMulter");
const { fileSchema, createModel } = require("../models/fileModel");
const mongoose = require("mongoose");
const { updateTimeAfterDownload } = require("../services/fileService");
//getting all files
router.get("/:name", async (req, res) => {
  try {
    let images = mongoose.model(req.params.name, fileSchema);
    const allFiles = await images.find();
    res.status(200).json(allFiles);
  } catch (error) {
    console.log(error);
    res.status(500).send("Došlo k chybě při stahování souboru.");
  }
});
//delete file
router.delete("/:name/:id", async (req, res) => {
  try {
    const fileId = req.params.id;
    let file = mongoose.model(req.params.name, fileSchema);
    if (!file) {
      return res.status(404).send("Soubor nebyl nalezen.");
    }
    file = await file.findByIdAndRemove(fileId);
    res.status(200).json({ message: "Soubor smazan." });
  } catch (err) {
    res.status(500).json({ message: err });
  }
});
//getting by id
router.get("/:name/:id", async (req, res) => {
  try {
    const fileId = req.params.id;
    console.log(fileId)
    let file = mongoose.model(req.params.name,fileSchema)
    file = await file.findById(fileId);
    if (!file) {
      return res.status(404).send("Soubor nebyl nalezen.");
    }
    await updateTimeAfterDownload(fileId);
    res.send(file.data);
  } catch (error) {
    console.log(error);
    res.status(500).send("Došlo k chybě při stahování souboru.");
  }
});
//post multiple files
router.post(
  "/:name",
  uploadFile.array("files", config.max_upload_multiple),
  async (req, res) => {
    try {
      const uploadedFiles = req.files;
      for (const uploadedFile of uploadedFiles) {
          const originalFileName = uploadedFile.originalname;
          await createModel(req.params.name, originalFileName, uploadedFile.buffer).save();
      }
      res.status(200).send("Soubory byly úspěšně uloženy do MongoDB.");
    } catch (error) {
      console.error(error);
      res.status(500).send("Došlo k chybě při nahrávání souboru.");
    }
  }
);

module.exports = router;
