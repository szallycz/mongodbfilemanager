const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
router.get('/', async (req, res) => {
    try {
      const collections = await mongoose.connection.db.listCollections().toArray();
      const collectionNames = collections.map(collection => collection.name);
      res.json({ collections: collectionNames });
    } catch (error) {
      console.error('Chyba při získávání seznamu kolekcí:', error);
      res.status(500).json({ error: 'Chyba při získávání seznamu kolekcí' });
    }
  });
module.exports = router;
