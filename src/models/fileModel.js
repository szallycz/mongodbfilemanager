const mongoose = require("mongoose");
const fileSchema = mongoose.Schema({
  size: {
    type: Number,
  },
  name: {
    require: true,
    type: String,
  },
  data: Buffer,
  originalName: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  numberOfDownloads: [
    {
      type: Date,
    },
  ],
});
function createModel(collection, originalName, buffer, size) {
  var file = mongoose.model(collection, fileSchema);
  var fileModel = new file({
    name: originalName,
    originalFileName: originalName,
    data: buffer,
    size: size,
  });
  return fileModel;
}
module.exports = { fileSchema, createModel };
