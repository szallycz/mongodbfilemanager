async function updateTimeAfterDownload(fileId) {
    try {
      const currentTime = Date.now();
      await fileSchema.findByIdAndUpdate(
        fileId,
        { $push: { numberOfDownloads: currentTime } },
        { new: true }
      );
    } catch (error) {}
  }
module.exports={updateTimeAfterDownload}